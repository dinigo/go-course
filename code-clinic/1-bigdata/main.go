package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
)

func main() {
	url := "https://raw.githubusercontent.com/lyndadotcom/LPO_weatherdata/master/Environmental_Data_Deep_Moor_2014.txt"
	res, err := http.Get(url)
	checkError(err)
	reader := csv.NewReader(res.Body)
	reader.Comma = '\t'
	reader.TrimLeadingSpace = true
	defer res.Body.Close()
	rows, err := reader.ReadAll()
	checkError(err)
	headers := rows[0]
	fmt.Println("Rows:", len(rows)-1) // substract the header row
	fmt.Println("Mean: ")
	for i := 1; i < len(headers); i++ {
		fmt.Printf("    %v: %.2f\n", headers[i], mean(rows, i))
	}
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func mean(rows [][]string, index int) float64 {
	var total float64
	for i, row := range rows {
		if i != 0 {
			num, err := strconv.ParseFloat(row[index], 64)
			checkError(err)
			total += num
		}
	}
	return total / float64(len(rows)-1)
}
