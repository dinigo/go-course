package main

import (
	"fmt"
	"math"
	"sync"
)

const threshold float64 = 500

func pixelDiff(n, h Pixel) float64 {
	var diff float64
	diff = math.Abs(float64(int(n.r) - int(h.r)))
	diff += math.Abs(float64(int(n.g) - int(h.g)))
	diff += math.Abs(float64(int(n.b) - int(h.b)))
	diff += math.Abs(float64(int(n.a) - int(h.a)))
	return diff
}

func compare(images []*Image) chan Result {
	fmt.Println("Comparing images...")
	var wg sync.WaitGroup
	ch := make(chan Result)
	for _, needle := range images {
		for _, haystack := range images {
			if needle.width <= haystack.width && needle.heigth <= haystack.heigth && needle.path != haystack.path {
				wg.Add(1)
				go comparePixels(needle, haystack, ch, &wg)
			}
		}
	}

	// when you have an async operation, like closing the channel, pending, you have to span a new goroutine
	go func() {
		wg.Wait()
		close(ch)
	}()
	return ch
}

func comparePixels(needle, haystack *Image, ch chan Result, parentWg *sync.WaitGroup) {
	var diff float64
	var wg sync.WaitGroup

	for i := 0; i < haystack.width*haystack.heigth; i++ {
		// current pixel in haystack
		x := i % haystack.width
		y := i / haystack.width

		// needle must fit
		if haystack.heigth-y < needle.heigth {
			break
		}
		if haystack.width-x < needle.width {
			continue
		}
		diff = pixelDiff(needle.pixels[0], haystack.pixels[i])
		if diff < threshold {
			wg.Add(1)
			go compareSequence(needle, haystack, i, ch, &wg)
		}
	}
	wg.Wait()
	parentWg.Done()
}

func compareSequence(n, h *Image, haystackIndexPixel int, ch chan Result, wg *sync.WaitGroup) {
	var counter int
	var accumulator uint64
	haystackStartPix := haystackIndexPixel
	pixelOffset := h.width - n.width
	for i := 0; i < n.width*n.heigth; i++ {
		diff := pixelDiff(n.pixels[i], h.pixels[haystackIndexPixel])
		// if previews row had less than 10 different pixels
		if i%n.width == 0 && i/n.width != 0 && counter < 10 {
			break
		}
		// row end, reset counter
		if i%n.width == 0 {
			counter = 0
		}
		// under threshold, increment counter
		if diff < threshold {
			counter++
		}
		// if we're at the end of the line, start next line in the right position
		if (i+1)%n.width == 0 {
			haystackIndexPixel += pixelOffset
		}
		haystackIndexPixel++
		accumulator += uint64(diff)
	}
	avgDiff := int(accumulator / uint64(n.heigth*n.width))
	ch <- Result{n, h, haystackStartPix, avgDiff}
	wg.Done()
}
