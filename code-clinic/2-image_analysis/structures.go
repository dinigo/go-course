package main

type Result struct {
	needle, haystack   *Image
	haystackStartPixel int
	avgDiff            int
}

type Pixel struct {
	r, g, b, a uint32
}

type Image struct {
	path   string
	pixels []Pixel
	width  int
	heigth int
}
