package main

import (
	"fmt"
	"image"
	"image/jpeg"
	"os"
	"path/filepath"
)

func loadImage(path string) image.Image {
	file, err := os.Open(path)
	ce(err)
	defer file.Close()
	img, err := jpeg.Decode(file)
	ce(err)
	return img
}

func main() {
	imagesFolder := "./code-clinic/2-image_analysis/images"
	paths := getPaths(imagesFolder)
	var images []*Image
	fmt.Printf("Processing %v images\n", len(paths))
	for _, path := range paths {
		img := decodeImage(path)
		images = append(images, img)
		fmt.Println("\t", img.width, "\tx\t", img.heigth, "\t-->\t", img.path)
	}
	ch := compare(images)
	for res := range ch {
		fmt.Printf("Source: %v, Crop: %v, avgDiff: %v, startPixel: %v\n", &res.haystack.path, res.needle.path, res.avgDiff, res.haystackStartPixel)
	}
}

func getPaths(root string) []string {
	var paths []string
	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if root != path && !info.IsDir() {
			paths = append(paths, path)
		}
		return nil
	})
	return paths
}

func decodeImage(path string) *Image {
	img := loadImage(path)
	pixels := getPixels(img)
	width := img.Bounds().Dx()
	height := img.Bounds().Dy()
	return &Image{path, pixels, width, height}
}

func getPixels(img image.Image) []Pixel {
	bounds := img.Bounds()
	pixels := make([]Pixel, bounds.Dx()*bounds.Dy())
	for i := 0; i < len(pixels); i++ {
		x := i % bounds.Dx()
		y := i / bounds.Dx()
		r, g, b, a := img.At(x, y).RGBA()
		p := Pixel{r, g, b, a}
		pixels[i] = p
	}
	return pixels
}

func ce(e error) {
	if e != nil {
		panic(e)
	}
}
