package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type data struct {
	Number int
	Text   string
}

func serveTemplate(res http.ResponseWriter, _ *http.Request) {
	tpl, err := template.ParseFiles("template.gohtml")
	ce(err)
	var data = []data{
		{1, "milk"},
		{3, "cheese"},
		{2, "melon"},
		{12, "yogurt"},
		{2, "peanut butter"},
		{7, "cookies"},
	}
	err = tpl.Execute(res, data)
	ce(err)
}

func main() {
	http.HandleFunc("/", serveTemplate)
	fmt.Printf("Running on http://localhost:8081")
	err := http.ListenAndServe(":8081", nil)
	ce(err)
}

func ce(err error) {
	if err != nil {
		panic(err)
	}
}
