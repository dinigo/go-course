package main

import "fmt"

func main() {
	str1 := "first line"
	str2 := "second line"
	str3 := "third line"

	strLength, err := fmt.Println(str1, str2, str3)
	if err == nil {
		fmt.Printf("String length in float: %.2f\n", float64(strLength))
	}

	isLong := strLength > 10
	num, err := fmt.Printf("My types are: %T, %T, %T, %T and %T\n", str1, str2, str3, strLength, isLong)
	if err == nil {
		fmt.Println("no error", num)
	}
}
