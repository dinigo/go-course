package main

import "fmt"

func main() {
	// store the address of an int
	var p *int
	if p != nil {
		fmt.Println("Vale of p:", *p)
	} else {
		fmt.Println("P is nil")
	}

	// store an actual value
	var v int = 34
	// copy the address of the value in the pointer
	// we can get the address with the ampersand
	p = &v

	if p != nil {
		fmt.Println("Vale of p:", p)  // address stored
		fmt.Println("Vale of p:", *p) // value in the address stored
		fmt.Println("Vale of p:", &p) // address of the pointer itself
	} else {
		fmt.Println("P is nil")
	}

	var value1 float64 = 3453.34
	pointer1 := &value1
	fmt.Println("value1: ", *pointer1)

	// operation using pointers
	*pointer1 = *pointer1 / 12
	fmt.Println("pointer1:", *pointer1)
	fmt.Println("value1:", value1)
}
