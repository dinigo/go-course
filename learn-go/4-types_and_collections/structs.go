package main

import "fmt"

type Book struct {
	Title  string
	Author string
	Rating int
}

func main() {
	lotr := Book{"The Lord of the Rings", "J. R. R. Tolkien", 1}
	fmt.Println(lotr)
	fmt.Printf("%+v\n", lotr) //prints field name too
}
