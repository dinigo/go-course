package main

import (
	"fmt"
	"sort"
)

func main() {
	// make() allocates memory while new() doesn't
	// memory is the delocated by the garbage collector

	// map <string:string>
	states := make(map[string]string)
	fmt.Println("Empty still:", states)
	states["AS"] = "Asturias"
	states["CAT"] = "Cataluña"
	states["AR"] = "Arganzuela"
	states["DE"] = "Dinamarca"
	states["GB"] = "Gran Bretaña"
	states["IS"] = "Islas Baleares"
	states["CAS"] = "Castilla y león"
	fmt.Println("Filled:", states)

	asturias := states["AS"]
	fmt.Println("Retrieve element:", asturias)

	delete(states, "AS")
	fmt.Println("After deleting element:", states)

	keys := make([]string, len(states))
	i := 0
	for k := range states {
		keys[i] = k
		i++
	}
	fmt.Println(keys)
	sort.Strings(keys)
	fmt.Println(keys)

	for k := range keys {
		fmt.Printf("%v --> %v\n", keys[k], states[keys[k]])
	}
}
