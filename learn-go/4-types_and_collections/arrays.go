package main

import (
	"fmt"
	"sort"
)

func main() {
	// definition
	var colors [3]string
	colors[0] = "red"
	colors[1] = "blue"
	colors[2] = "green"
	// basic array operations
	fmt.Println("whole array", colors)
	fmt.Println("single element", colors[1])
	fmt.Println("length:", len(colors))
	//slices are resizeable
	var rainbow = []string{"red", "green", "purple", "blue"}
	rainbow = append(rainbow, "yellow")
	fmt.Println("rainbow:", rainbow)
	fmt.Println("sliced:", rainbow[1:len(rainbow)])
	fmt.Println("sliced:", rainbow[:len(rainbow)-1])

	numbers := make([]int, 3, 3)
	numbers[0] = 2
	numbers[1] = 2
	numbers[2] = 1
	numbers[3] = 3
	numbers[4] = 4
	fmt.Println("custom slice with prefilled space", numbers)

	numbers = append(numbers, 323)
	fmt.Println(numbers)
	fmt.Println(cap(numbers))
	sort.Ints(numbers)
	fmt.Println(numbers)
}
