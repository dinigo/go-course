package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	content := "hola mundo"

	file, err := os.Create("./myfile.txt")
	checkError(err)
	defer file.Close()
	defer fmt.Println("close file")

	ln, err := io.WriteString(file, content)
	checkError(err)

	fmt.Printf("all good, written %v chars\n", ln)

	bytes := []byte(content)
	ioutil.WriteFile("./myfile.txt", bytes, 644)

}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
