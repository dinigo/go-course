package main

import (
	"fmt"
	"net/http"
)

type Hello struct{}

func (h Hello) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "<h1>Hello world</h1>")
}

func main() {
	var h Hello
	err := http.ListenAndServe("localhost:8080", h)
	checkError(err)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
