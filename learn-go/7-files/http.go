package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	url := "https://jsonplaceholder.typicode.com/todos/"
	content := ContentFromServer(url)
	fmt.Println(content)
	tours := Parse(content)
	fmt.Println(tours)
}

type Task struct {
	Id        int
	UserId    int
	Title     string
	Completed bool
}

func ContentFromServer(url string) string {
	res, err := http.Get(url)
	defer res.Body.Close()
	bytes, err := ioutil.ReadAll(res.Body)
	checkError(err)
	return string(bytes)
}

func Parse(content string) []Task {
	var taskCollection []Task
	json.Unmarshal([]byte(content), &taskCollection)
	return taskCollection
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
