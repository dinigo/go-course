package main

import (
	"fmt"
	"strings"
)

func main() {
	str1 := "implicitly typed string"
	fmt.Printf("str1: %v:%T\n", str1, str1)
	str2 := "explicitly typed string"
	fmt.Printf("strs: %v:%T\n", str2, str2)
	fmt.Println(strings.ToUpper(str1))
	fmt.Println(strings.Title(str1))

	lvalue := "hello"
	uvalue := "Hello"

	fmt.Println("Equal?", uvalue == lvalue)
	fmt.Println("Equal folded?", strings.EqualFold(lvalue, uvalue))
	fmt.Println("Contains str1 exp?", strings.Contains(str1, "exp"))
	fmt.Println("Contains str2 exp?", strings.Contains(str2, "exp"))
}
