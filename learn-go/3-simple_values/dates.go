package main

import (
	"fmt"
	"time"
)

func main() {
	t := time.Date(2013, time.November, 10, 30, 0, 0, 0, time.UTC)
	fmt.Println("Custom date:", t)

	now := time.Now()
	fmt.Println("The time now is:", now)
	fmt.Println("The month now is:", now.Month())
	fmt.Println("The day now is:", now.Day())

	tomorrow := now.AddDate(0, 0, 1)
	fmt.Println("Tomorrow is:", tomorrow)

	longFormat := "Monday, November 2, 2014"
	fmt.Println("Format a string", tomorrow.Format(longFormat))
}
