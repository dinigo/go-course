package main

import (
	"fmt"
	"math"
	"math/big"
)

func main() {
	i1, i2, i3 := 12, 14, 54
	intSum := i1 + i2 + i3
	fmt.Println("Integer sum:", intSum)

	var b1, b2, b3, bigSum big.Float
	b1.SetFloat64(3234.32)
	b2.SetFloat64(3234.32)
	b3.SetFloat64(3234.32)

	bigSum.Add(&b1, &b2).Add(&bigSum, &b3)
	fmt.Println("BigSum: ", &bigSum)

	circleRadius := 15.5
	circunference := circleRadius * math.Pi
	fmt.Printf("Circunference: %.2f\n", circunference)
}
