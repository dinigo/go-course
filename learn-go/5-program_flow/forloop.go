package __program_flow

import "fmt"

func main() {
	sum := 1
	colors := []string{"RED", "GREN", "BLUE"}
	fmt.Println(colors)

	for i := 0; i < len(colors); i++ {
		sum += i
	}
	fmt.Println("some sum:", sum)

	for i := range colors {
		fmt.Println("color index:", i)
	}

	for sum < 1000 {
		fmt.Println("SUM:", sum)
		sum += sum
		if sum > 200 {
			goto endofprogram
		}
	}

endofprogram:
	fmt.Println("end of program")
}
