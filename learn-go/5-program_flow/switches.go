package __program_flow

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix())
	dow := rand.Intn(6) + 1
	fmt.Println("DAY", dow)

	var result string
	switch dow {
	case 6:
		fallthrough
	case 7:
		result = "its weekend"
	default:
		result = "its work day"
	}

	fmt.Println(result)

}
