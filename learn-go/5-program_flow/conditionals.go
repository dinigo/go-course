package __program_flow

import (
	"fmt"
)

func main() {
	var x float64 = 42
	var result string
	if x < 0 {
		result = "is negative"
	} else if x == 0 {
		result = "is cero"
	} else {
		result = "is positive"
	}

	fmt.Println(result)
}
