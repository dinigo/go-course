package main

import "fmt"

type Animal interface {
	Speak() string
}

type Dog struct{}

func (d Dog) Speak() string {
	return "waf"
}

type Cat struct{}

func (c Cat) Speak() string {
	return "meow"
}

type Cow struct{}

func (c Cow) Speak() string {
	return "moooo"
}

func main() {
	animals := []Animal{Dog{}, Cat{}, Cow{}}
	for _, animal := range animals {
		sound := animal.Speak()
		fmt.Println(sound)
	}
}
