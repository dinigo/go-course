package main

import (
	"fmt"
)

func main() {
	dosomething()

	sum := addValues(23, 32)
	fmt.Println("SUM:", sum)

	sum = addAllValues(23, 32, 4232, 43, 32)
	fmt.Println("SUM:", sum)

}

func dosomething() {
	fmt.Println("Prints a message")
}

func addValues(val1, val2 int) int {
	return val1 + val2
}

func addAllValues(values ...int) int {
	sum := 0
	for i := range values {
		sum += values[i]
	}
	return sum
}
