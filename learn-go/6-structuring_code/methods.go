package main

import "fmt"

func main() {
	// declare the book
	b := Book{"The Lord of The Flies", 8}
	b.Sign()

	// change the title
	b.Title = "The lord of the Rings"
	b.Sign()
}

type Book struct {
	Title  string
	Rating int
}

func (b Book) Sign() {
	fmt.Println(b, "is now signed by it's author")
}
