package main

import "fmt"

func main() {
	n1, l1 := FullName("Daniel", "Inigo")
	fmt.Printf("name: %v, length: %v\n", n1, l1)

	n2, l2 := FullName("Daniel", "Inigo")
	fmt.Printf("name: %v, length: %v\n", n2, l2)

}

func FullName(f, l string) (string, int) {
	full := f + " " + l
	length := len(full)
	return full, length
}

func FullNameSimpleReturn(f, l string) (full string, length int) {
	full = f + " " + l
	length = len(full)
	return
}
