package main

import "fmt"

func main() {
	defer fmt.Println("first defered")
	fmt.Println("firs statement")
	defer fmt.Println("statement 1")
	defer fmt.Println("statement 2")
	defer fmt.Println("statement 3")
	myfunc()
	defer fmt.Println("statement 4")
	defer fmt.Println("statement 5")
	fmt.Println("last statement")
}

func myfunc() {
	defer fmt.Println("defered inside func")
	fmt.Println("not defered inside func")
}
