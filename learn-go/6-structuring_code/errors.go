package main

import (
	"errors"
	"fmt"
	"os"
)

func main() {
	f, err := os.Open("myfile.ext")

	if err == nil {
		fmt.Println(f)
	} else {
		fmt.Println(err.Error())
	}

	myerror := errors.New("this is a new error")
	fmt.Println(myerror.Error())

	assistants := map[string]bool{
		"mike":  true,
		"hanna": false,
	}
	attendent, ok := assistants["m"]
	if ok {
		fmt.Println("attendent", attendent)
	} else {
		fmt.Println("nope!")
	}

}
